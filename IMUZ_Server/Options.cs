﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;

namespace IMUZ_Server
{
    public class Options
    {
        [Option('p', "port",HelpText ="Specify communication port", Default  = "COM4")]
        public string portName { get; set; }
        [Option('i',"id",HelpText ="sensor id",Default =1)]
        public int id { get; set; }
        
        
    }
}
