﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Zmp.Imuz.Communication;
using IMUZ_Server.DataSource;
using IMUZ_Server.Usecase;

namespace IMUZ_Server
{
    class Program
    {
        static void Main(string[] args)
        {
            CommandLine.Parser.Default.ParseArguments<Options>(args)
                .WithParsed(options => Run(options))
                .WithNotParsed(err => {
                    Console.WriteLine(err);
                    Environment.Exit(-1);
                });

        }
        static void Run(Options options)
        {
            var datasource = new SensorDataSource(options.portName);
            var usecase = new ReceiveAccelerometerDataUsecase();
            usecase.Execute(datasource);
            Console.ReadLine();
        }
    }
}
