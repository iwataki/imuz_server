﻿using Redux;
using Zmp.Imuz.Communication;

namespace IMUZ_Server.Redux.Store
{
    static public class Store
    {
        static IStore<AccelerometerData> AccelerometerDataStore;
        static IStore<GyroscopeData> GyroscopeDataStore;
        static IStore<CompassData> CompassDataStore;
    }
}