﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redux;
using Zmp.Imuz.Communication;

namespace IMUZ_Server.Redux.Actions
{
    class AddNewAccelerometerData:IAction
    {
        public AccelerometerData newdata { get; set; }
    }
}
