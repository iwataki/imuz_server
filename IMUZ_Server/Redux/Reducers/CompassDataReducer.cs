﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redux;
using Zmp.Imuz.Communication;

namespace IMUZ_Server.Redux.Reducers
{
    class CompassDataReducer
    {
        public static CompassData Execute(CompassData state, IAction action)
        {
            if (action is Redux.Actions.AddNewCompassData)
            {
                return ((Actions.AddNewCompassData)action).newdata;
            }
            return state;
        }
    }
}
