﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redux;
using Zmp.Imuz.Communication;

namespace IMUZ_Server.Redux.Reducers
{
    class AccelerometerDataReducer
    {
        public static AccelerometerData Execute(AccelerometerData state,IAction action)
        {
            if(action is Redux.Actions.AddNewAccelerometerData)
            {
                return ((Actions.AddNewAccelerometerData)action).newdata;
            }
            return state;
        }
    }
}
