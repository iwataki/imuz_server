﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redux;
using Zmp.Imuz.Communication;

namespace IMUZ_Server.Redux.Reducers
{
    class GyroscopeDataReducer
    {
        public static GyroscopeData Execute(GyroscopeData state, IAction action)
        {
            if (action is Redux.Actions.AddNewGyroscopeData)
            {
                return ((Actions.AddNewGyroscopeData)action).newdata;
            }
            return state;
        }
    }
}
