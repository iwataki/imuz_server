﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMUZ_Server.DataSource;

namespace IMUZ_Server.Usecase
{
    class ReceiveAccelerometerDataUsecase
    {
        public  IDisposable Execute(SensorDataSource datasource)
        {
            return datasource.getAccelerometerDataSource().Subscribe(val =>
            {
                Console.Write("x={0},y={1},z={2}\n", val.acc[0], val.acc[1], val.acc[2]);
            });
        }
    }
}
