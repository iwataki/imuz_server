﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zmp.Imuz.Communication;
using Redux;

namespace IMUZ_Server.DataSource
{
    class SensorDataSource
    {
        private ICommunicationPort port;

        public SensorDataSource(string portName)
        {
            var names=CommunicationManager.GetPortNames();
            foreach(var name in names){
                Console.WriteLine("Portname={0}\n", name);
            }
            port = CommunicationManager.CreateCommunicationPort(portName,PortType.CANUSBz);
            if (port.Open()){
                Console.WriteLine("Port open OK");
            }else
            {
                Console.WriteLine("Port open fail");
            }
            
        }
        private void portOpenClosedHandler<T>(IObserver<T> o)
        {
            if (!port.IsOpen)
            {
                o.OnCompleted();
            }
        }

        public IObservable<AccelerometerData> getAccelerometerDataSource()
        {
            return System.Reactive.Linq.Observable.Create<AccelerometerData>(o =>
            {
                port.MsgAccelerometerReceived += (sender, data) => {
                    o.OnNext(data);
                    Console.WriteLine("acc x={0},y={1},z={2}", data.acc[0], data.acc[1], data.acc[2]);
                }; 
                port.OpenOrClosed += sender => portOpenClosedHandler(o);
                return System.Reactive.Disposables.Disposable.Empty;
            });
        }
        public IObservable<GyroscopeData> getGyroscpeDataSource()
        {
            return System.Reactive.Linq.Observable.Create<GyroscopeData>(o =>
            {
                port.MsgGyroscopeReceived += (sender, data) => o.OnNext(data);
                port.OpenOrClosed += sender => portOpenClosedHandler(o);
                return System.Reactive.Disposables.Disposable.Empty;
            });
        }
        public IObservable<CompassData> getCompassDataSource()
        {
            return System.Reactive.Linq.Observable.Create<CompassData>(o =>
            {
                port.MsgCompassReceived += (sender,data)=>o.OnNext(data);
                port.OpenOrClosed += sender => portOpenClosedHandler(o);
                return System.Reactive.Disposables.Disposable.Empty;
            });
        }
    }
}
